String sketchName = "LittleGarden";
int  sketchVersion = 101;
int latestVersion = 0;

/*
   @file
   Sketch for CO2, temperature and humidity sensor.

   @author Javier Mansilla (javier@mansilla.info)

*/

// Import Settings
#include "sensor3.h"       // import variables

#include <BME280I2C.h>
#include <Wire.h>

#define SERIAL_BAUD 115200

BME280I2C bme;

#define MYALTITUDE  150.50

// Serial communication
#include <SoftwareSerial.h>

// JSON parser
#include <ArduinoJson.h>

// WIFI communication
#include <ESP8266WiFi.h>
//#include <ESP8266WiFiMulti.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <ESP8266httpUpdate.h>


SoftwareSerial mySerial(CO2RX, CO2TX);  // RX, TX respectively
WiFiManager wifiManager;


// DHT
#include "DHT.h"
#define DHTTYPE DHT21   // DHT 22  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);



// Variables
int value = 0;
int ppm = 0;
float h = 0;
float t = 0;
float tempStdDev = 0;
float pressure = 0;
int iterations = 1000; // High value to ensure the data delivery to the server
int CO2sensorMinutesON = 0;
boolean CO2warmingUp = true;




////////////////////////////////////////////////////
//    SETUP   //
////////////////////////////////////////////////////
void setup() {

  Wire.begin(D5,D6);     // initialize I2C that connects to sensor
  while(!bme.begin())
  {
    Serial.println("Could not find BME280 sensor!");
    delay(1000);
  }                                                   // initalize bme280 sensor


  // Initialize the switch pins and set them ON

  pinMode(CORELAY, OUTPUT);
  pinMode(CO2RX, OUTPUT);
  pinMode(CO2TX, OUTPUT);


  digitalWrite(CORELAY, LOW);
  digitalWrite(CO2RX, LOW);
  digitalWrite(CO2RX, LOW);

  // Initialize temp/hum sensor
  dht.begin();


  // Initialize serial pipes
  Serial.begin(115200);
  mySerial.begin(9600);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  // Set a timeout of 30 seconds (try 60 times) for connecting to the network
  int timeout = 0;

  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  while (WiFi.status() != WL_CONNECTED && timeout < 60) {
    delay(500);
    Serial.print(timeout);
    timeout++;

    // After trying 60 times, try with another network
    if (timeout == 60) {
      continue;
    }
  }
  // Config the wifi connection
  // We start by connecting to a WiFi network
  const char* networkName = "Little Garden - Sensor ";
  wifiManager.autoConnect(networkName, "12345678");

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  delay(5000);
  digitalWrite(CORELAY, HIGH);
}





////////////////////////////////////////////////////
//    LOOP    //
////////////////////////////////////////////////////
void loop() {

  // Start a new iteration
  iterations++;
  CO2sensorMinutesON++;
  int start = millis();
  printBME280Data(&Serial);
  //readBMESensor();

  // Read the CO2 sensor and wait
  readMHZ16();
  delay(500);

  readDHT(5);
  int index = 0;
  // Read DHT up to 10 times if values are both 0, just to avoid reading errors
  while (((h == 0 && t == 0) || tempStdDev > 2) && index < 10 ) {
    Serial.println(" Trying again...");
    readDHT(5);
    index++;
  }

  /*
    restablishingConnection();
    if (WiFi.status() != WL_CONNECTED) {
      ESP.deepSleep(5 * 1000000);
    }

    Serial.print("connecting to ");
    Serial.println(host);
  */

  if (iterations >= period) {
    sendDataToServer();
    if (latestVersion > sketchVersion) {
      updateSketch();
    }
    iterations = 0;
  }


  // If CO2 sensor has been ON for 10 hours, reset it
  if(CALIBRATED && (CO2sensorMinutesON >= 600 || ppm > CO2range)) {
    digitalWrite(CORELAY, LOW);
    delay (15000);
    digitalWrite(CORELAY, HIGH);

    CO2sensorMinutesON = 0;
  }

  int duration = millis() - start;

  // Wait for 60 (or 120, if loop took longer than one minute) seconds minus execution time
  if (duration > 60000) {
    delay(120000 - duration);
  }
  else {
    delay(60000 - duration);
  }

}


////////////////////////////////////////////////////
//    Send Data to Server    //
////////////////////////////////////////////////////
void sendDataToServer() {
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;

  int connectionFails = 0;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    delay(10000);

    connectionFails++;

    if (connectionFails == 10) {
      ESP.deepSleep(5 * 1000000);
    }
    return;
  }


  // We now create a URI for the request
  value++;
  //String url = "/api/insertdata.php?hash=littlegarden&s=" + sensorID + "&t=" + String(t) + "&h=" + String(h) + "&co2=" + String(ppm) + "&p=" + String(pressure) + "&times=" + String(value);
  //String url = "/insertdata.php?hash=helsienifarm&s=" + sensorID + "&t=" + String(t) + "&h=" + String(h) + "&co2=" + String(ppm) + "&p=0&times=" + String(value);
  String url = path + "?hash=" + hash + "&s=" + sensorID + "&t=" + String(t) + "&h=" + String(h) + "&co2=" + String(ppm) + "&p=" + String(pressure) + "&times=" + String(value) + "&version=" + String(sketchVersion);

  Serial.print("Requesting URL: ");
  Serial.println(url);



  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");


  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 10000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  // Read all the lines of the reply from server and print them to Serial
  String serverResponse = "";
  while (client.available()) {
    String line = client.readStringUntil('\r');
    serverResponse += line;
    //Serial.print(line);
  }

  int firstOpeningBracket = serverResponse.indexOf('{', 0);
  int lastClosingBracket = serverResponse.indexOf('}', 0);

  Serial.print(firstOpeningBracket);
  Serial.print(" - ");
  Serial.println(lastClosingBracket);

  String JSON = serverResponse.substring(firstOpeningBracket, lastClosingBracket + 1);
  Serial.print(JSON);
  DynamicJsonBuffer  jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(JSON);

  if (root.success()) {
    int p = (int)root["interval"];
    latestVersion = (int)root["latestVersion"];

    Serial.println(latestVersion);

    if (p > 59000) {
      period = p / 60000;
      Serial.print("New period is: ");
      Serial.println(period);
    }
  }
  else {
    Serial.println("parseObject() failed");
  }


  Serial.println();
  Serial.println("closing connection");
  Serial.println();
  Serial.print("Sensor data sent: ");
  Serial.print(value);
  Serial.println(" times");

}



void updateSketch() {
  // wait for WiFi connection
  if (WiFi.status() == WL_CONNECTED) {

    WiFiClient client;

    // The line below is optional. It can be used to blink the LED on the board during flashing
    // The LED will be on during download of one buffer of data from the network. The LED will
    // be off during writing that buffer to flash
    // On a good connection the LED should flash regularly. On a bad connection the LED will be
    // on much longer than it will be off. Other pins than LED_BUILTIN may be used. The second
    // value is used to put the LED on. If the LED is on with HIGH, that value should be passed
    ESPhttpUpdate.setLedPin(LED_BUILTIN, LOW);

    // Add optional callback notifiers
    ESPhttpUpdate.onStart(update_started);
    ESPhttpUpdate.onEnd(update_finished);
    ESPhttpUpdate.onProgress(update_progress);
    ESPhttpUpdate.onError(update_error);

    String filepath = "http://" + host + "/sketches/sensor" + sensorID + "-v" + String(latestVersion) + ".bin";

    Serial.println(filepath);

    t_httpUpdate_return ret = ESPhttpUpdate.update(client, filepath);

    switch (ret) {
      case HTTP_UPDATE_FAILED:
        Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
        break;

      case HTTP_UPDATE_NO_UPDATES:
        Serial.println("HTTP_UPDATE_NO_UPDATES");
        break;

      case HTTP_UPDATE_OK:
        Serial.println("HTTP_UPDATE_OK");
        break;
    }
  }
}


void update_started() {
  Serial.println("CALLBACK:  HTTP update process started");
}

void update_finished() {
  Serial.println("CALLBACK:  HTTP update process finished");
}

void update_progress(int cur, int total) {
  Serial.printf("CALLBACK:  HTTP update process at %d of %d bytes...\n", cur, total);
}

void update_error(int err) {
  Serial.printf("CALLBACK:  HTTP update fatal error code %d\n", err);
}



////////////////////////////////////////////////////
//    readDHT    //
////////////////////////////////////////////////////
/*
void readDHT() {

  // Initialize old (globals) and new variables
  float t1, t2; // Temperature will be read twice
  float h1, h2; // Humidity will be read twice
  t = 0;        // Reset temperature
  h = 0;        // Reset humidity


  // Read temperature as Celsius (the default)
  h1 = dht.readHumidity();
  t1 = dht.readTemperature();

  // Wait, it's a slow sensor
  delay(1000);

  // If values are not a number, set both as 0
  if (isnan(h1) || isnan(t1)) {
    h1 = 0;
    t1 = 0;
  }

  // Read sesor again to check if values are consistent
  h2 = dht.readHumidity();
  t2 = dht.readTemperature();

  // Wait, it's a slow sensor
  delay(1000);

  // If values are not a number, set both as 0
  if (isnan(h2) || isnan(t2)) {
    h2 = 0;
    t2 = 0;
  }

  Serial.println(t1 - t2);
  Serial.println(h1 - h2);


  if (abs(t1 - t2) < 0.3 && abs(h1 - h2) < 1) {
    h = h1;
    t = t1;
  }

}
*/


//////////////////////////////////////////////////////////////////

void printBME280Data(Stream* client) {
   float temp(NAN), hum(NAN), pres(NAN);

   BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
   BME280::PresUnit presUnit(BME280::PresUnit_Pa);

   bme.read(pressure, temp, hum, tempUnit, presUnit);

   client->print("Temp: ");
   client->print(temp);
   client->print("°"+ String(tempUnit == BME280::TempUnit_Celsius ? 'C' :'F'));
   client->print("\t\tHumidity: ");
   client->print(hum);
   client->print("% RH");
   client->print("\t\tPressure: ");
   pressure = pressure / 100;
   client->print(pressure);
   client->println(" Pa");

   delay(1000);
}



////////////////////////////////////////////////////
//    Read CO2 sensor    //
////////////////////////////////////////////////////
void readMHZ16() {

  char response[9];
  ppm = 0;
  int times = 0;


  do {
    // Restarting Serial communication with CO2 sensor to ensure it works fine
    SoftwareSerial mySerial(CO2RX, CO2TX);  // RX, TX respectively
    mySerial.begin(9600);
    mySerial.write(cmd, 9);
    delay(1000);
    mySerial.readBytes(response, 9);
    delay(1000);

    int responseHigh = (int) response[2];
    int responseLow = (int) response[3];
    ppm = (256 * responseHigh) + responseLow;

    Serial.print("CO2 concentration is: ");
    Serial.print(ppm);
    Serial.println(" ppm");
    times++;
    Serial.println(times);
  } while (((int)response[1] != 134 || (ppm == 410 && CO2warmingUp == true)) && times <= 10 );

  CO2warmingUp == false;

  // Sometimes, In case the value it's too high, the module needs to be resetted
  if (ppm > CO2range || ppm == 0 || times >= 10) {
    //ESP.deepSleep(5 * 1000000);
  }
}


/**
 * readDHT()
 * Helper function that takes 'times' data samples
 * from DHT sensors and stores the average value
 */
void readDHT(int times) {
  float * tempArr = new float[times] {};
  float temp_t = 0;
  float temp_h = 0;
  for(int i = 0; i < times; i++) {
    float parcialTemp = dht.readTemperature();
    temp_t += parcialTemp; // Gets the values of the temperature
    temp_h += dht.readHumidity();    // Gets the values of the humidity
    tempArr[i] = parcialTemp;
    Serial.print(parcialTemp);
    Serial.print(" - ");
    delay(1000);
  }

  t = temp_t/times;  
  getStdDev(tempArr, times, temp_t/times);
  h = temp_h/times;
}



/*
 * Get the mean from an array of ints
 */
float getMean(float * val, int arrayCount) {
  float total = 0;
  Serial.println("");
  for (int i = 0; i < arrayCount; i++) {
    total = total + val[i];
    Serial.print(val[i]);
    Serial.print(" - ");
  }
  Serial.println("");
  float avg = total/arrayCount;
  return avg;
}

/*
 * Get the standard deviation from an array of floats
 */
void getStdDev(float * val, int arrayCount, float avg) {
  //float avg = getMean(val, arrayCount);
  Serial.print("Average value is: ");
  Serial.println(String(avg));
  float total = 0;
  for (int i = 0; i < arrayCount; i++) {
    total = total + (val[i] - avg) * (val[i] - avg);
    Serial.print(val[i]);
    Serial.print("(");
    Serial.print(total);
    Serial.print(") - ");
  }

  float variance = total/arrayCount;
  tempStdDev = sqrt(variance);
  Serial.print("Standard deviation is: ");
  Serial.println(String(tempStdDev));
  ;
}
